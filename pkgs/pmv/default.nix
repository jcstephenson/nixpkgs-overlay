{ pkgs ? import <nixpkgs> {} }:
let commit = "4f40e77d0e6d865f6d7b8b0f8ac8f69625613718";
in
pkgs.buildGoModule rec {
  pname = "pmv";
  version = "3.14.0";
  src = fetchGit {
    url = "https://gitlab.com/gitlab-com/gl-infra/pmv.git";
    ref = "v${version}";
    rev = commit;
    # hash = pkgs.lib.fakeHash;
  };

  ldflags = let
    modulePath = "gitlab.com/gitlab-com/gl-infra/pmv";
  in [
    "-s" "-w"
    "-X ${modulePath}/cmd.version=${version}"
    "-X ${modulePath}/cmd.Commit=${commit}"
  ];

  vendorHash = "sha256-augI5bXk7qVoVBbGCdKDIXNYWjde0dk9tMjwoCyqOQw=";
  # vendorHash = pkgs.lib.fakeHash;

  meta = with pkgs.lib; {
    description = "Password Management Vault. or maybe Poor Man's Vault";
    homepage = "https://gitlab.com/gitlab-com/gl-infra/${pname}";
    license = pkgs.lib.licenses.mit;
    maintainers = [
      "jstephenson@gitlab.com"
    ];
  };
}
