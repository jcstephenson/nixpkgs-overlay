{ pkgs ? import <nixpkgs> {} }:
let commit = "a99c97adf93f1e7a8dfd7e1c6467442ca782d2d8";
    modulePath = "gitlab.com/gitlab-com/gl-infra/jsonnet-tool";
in
pkgs.buildGoModule rec {
  pname = "jsonnet-tool";
  version = "1.12.0";
  src = pkgs.fetchFromGitLab {
    owner = "gitlab-com/gl-infra";
    repo = pname;
    rev = "v${version}";
    hash = "sha256-qzIZPiVas4iPNgu3godTFonStpWulTWXL6StimXpY30=";
  };

  ldflags = [
    "-s" "-w"
    "-X ${modulePath}/cmd.version=${version}"
    "-X ${modulePath}/cmd.commit=${commit}"
    "-X ${modulePath}/cmd.date=unset"
  ];

  vendorHash = "sha256-thi6KoK1IQuqS4vpJlruV9WSaSWSKEsW+YtmtE8Olis=";

  meta = with pkgs.lib; {
    description = "A simple tool, primarily used to build YAML configuration files from Jsonnet source configurations.";
    homepage = "https://gitlab.com/gitlab-com/gl-infra/${pname}";
    license = pkgs.lib.licenses.mit;
    maintainers = [
      "jstephenson@gitlab.com"
    ];
  };
}
