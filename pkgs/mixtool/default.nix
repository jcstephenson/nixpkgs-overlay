{ pkgs ? import <nixpkgs> {} }:
let commit = "1aace356d01747eea16a2677ed13bdcbbd4bb082";
    modulePath = "monitoring-mixins/mixtool";
    shortCommit = pkgs.lib.strings.concatStrings (pkgs.lib.lists.take 6 (pkgs.lib.strings.stringToCharacters commit));
in
pkgs.buildGoModule rec {
  pname = "mixtool";
  version = "0.1.0-pre";
  src = pkgs.fetchFromGitHub {
    owner = "monitoring-mixins";
    repo = pname;
    rev = commit;
    hash = "sha256-F8fzvJSRxrOhhgdFlMfaOaWoWpoqZ3u/RxVsNKVAjNY=";
  };

  ldflags = [
    "-s" "-w"
    "-X main.version=${version}+${shortCommit}"
  ];

  vendorHash = "sha256-SdWHBRd3yVwyO8mmURqV6B11Ph/xWFKSUHlEnqTdOLo=";

  meta = with pkgs.lib; {
    description = "mixtool is a helper for easily working with jsonnet mixins.";
    homepage = "https://github.com/${modulePath}";
    license = pkgs.lib.licenses.asl20;
    maintainers = [
      "jstephenson@gitlab.com"
    ];
  };
}
